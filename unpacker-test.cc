#include <cstdio>
#include <iostream>
#include <fstream>
#include <cstdint>
#include <vector>
#include <array>
#include <utility>
#include <bitset>

#include "AMCEvent.h"

void dump(AMCEvent* ev, std::ofstream& dumfile){

    std::vector<GEB> gebs = ev->gebs();
    for (unsigned short i = 0; i < gebs.size(); ++i) {
        GEB geb = gebs.at(i);
        std::vector<VFAT> vfats = geb.vfats();
        for (unsigned short j = 0; j < vfats.size(); ++j) {
            VFAT vfat = vfats.at(j);
            std::bitset<64> ms(vfat.msData());
            std::bitset<64> ls(vfat.lsData());
            std::string ss = ms.to_string();
            ss += ls.to_string();

            int ch = 0;
            for (char const &c: ss) {
                
                dumfile << ev->runNumber() << ",";
                dumfile << ev->evtNumber() << ",";
                dumfile << int(ev->swFED()) << ",";
                dumfile << int(ev->slot()) << ",";
                dumfile << int(geb.inputId()) << ",";
                dumfile << vfat.position() << ",";
                dumfile << ch << ",";
                dumfile << c << std::endl;
                ++ch;
            }
        }
    }
}

int main (int argc, char** argv) {
    if (argc<4) {
        std::cout << "Usage: run [input raw file] [output text file] [number of events to unpack]" << std::endl; 
        return 0;
    }
    std::string ifile = argv[1];
    std::string ofile = argv[2];
    int event_number = atoi(argv[3]);

    /* TODO: read binary file and first n events */

    std::FILE *f;
    std::cout << "Reading from " << ifile << std::endl;
    f = std::fopen(ifile.c_str(), "rb");

    std::size_t sz;

    uint32_t word;
    uint32_t run, lumi, evt, size;
    uint64_t *buffer;

    std::cout << "Writting to: " << ofile << std::endl;
    std::ofstream dumpfile;
    dumpfile.open(ofile.c_str());
    dumpfile << "run," << "event," << "FED," << "slot,";
    dumpfile << "geb," << "vfat," << "channel," << "fired";
    dumpfile << std::endl;

    while (true) {
        sz = std::fread(&word, sizeof(uint32_t), 1, f);
        if (sz == 0) break;
        std::fread(&word, sizeof(uint32_t), 1, f);
        run = word;
        std::fread(&word, sizeof(uint32_t), 1, f);
        lumi = word;
        std::fread(&word, sizeof(uint32_t), 1, f);
        evt = word;
        std::fread(&word, sizeof(uint32_t), 1, f);
        size = word;
        std::fread(&word, sizeof(uint32_t), 1, f);

        buffer = (uint64_t*) malloc(sizeof(uint64_t)*(size/8));
        std::fread(buffer, sizeof(uint64_t), size/8, f);
        AMCEvent* amc = new AMCEvent(run, evt, size/8);
        amc->unpack(buffer);
        dump(amc, dumpfile);
        free(buffer);
    }
    dumpfile.close();
    return 0;
}

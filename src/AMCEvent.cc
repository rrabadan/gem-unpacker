#include "AMCEvent.h"

void AMCEvent::setAMCheader1(uint64_t word)
{
    m_slot = 0xf & (word >> 56); 
    m_eventCount = 0xffffff & (word >> 32);
    m_bunchCount = 0xfff & (word >> 20);
    m_eventLenHdr = 0xfffff & word;
}
    
void AMCEvent::setAMCheader2(uint64_t word)
{
    m_format = 0xf & (word >> 60);
    m_orbitCount = 0xffffffff & (word >> 16);
    m_swSlot = 0xf & (word >> 12);
    m_swFED = 0xfff & word;
}
    
void AMCEvent::setAMCtrailer(uint64_t word)
{
    m_crc = 0xffffffff & (word >> 32);
    m_eventCountTrl = 0xff & (word >> 24);
    m_eventLenTrl = 0xfffff & word;
}

void AMCEvent::setGEMheader(uint64_t word)
{
    m_davList = 0xffffff & (word >> 40);
    m_davCount = 0x1f & (word >> 11);
    m_gemFormat = 0x7 & (word >> 8);
    m_vpType = 0xf & (word >> 4);
    m_tts = 0xf & word;
}

void AMCEvent::setGEMtrailer(uint64_t word)
{
    m_linkTMO = word >> 40;
    m_runType = 0xf & (word >> 35);
    m_runParams = 0xffffff & (word >> 8);
    m_backPressure = 0x1 & (word >> 7);
    m_mmcmLocked = 0x1 & (word >> 6);
    m_daqClockLocked = 0x1 & (word >> 5);
    m_daqReady = 0x1 & (word >> 4);
    m_bc0Locked = 0x1 & (word >> 3);
    m_l1aF = 0x1 & (word >> 1);
    m_l1aNF = 0x1 & word ;
}

void AMCEvent::unpack(uint64_t *word)
{
    // headers
    this->setAMCheader1(*word);
    this->setAMCheader2(*(++word));
    this->setGEMheader(*(++word));

    // GEB data
    for(uint8_t j = 0; j < this->davCount(); ++j) {
        GEB * gebdata = new GEB();
        gebdata->setChamberHeader(*(++word));

        //VFAT data
        for (uint16_t k = 0; k < gebdata->vfatWdCnt() / 3; ++k) {
            VFAT * vfat = new VFAT();
            vfat->readFirstWord(*(++word));
            vfat->readSecondWord(*(++word));
            vfat->readThirdWord(*(++word));
            gebdata->addVFAT(*vfat);
        }
        gebdata->setChamberTrailer(*(++word));
        this->addGEB(*gebdata);
    }
    this->setGEMtrailer(*(++word));
    this->setAMCtrailer(*(++word));
}

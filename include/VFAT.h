#ifndef VFAT_H
#define VFAT_H

class VFAT {
public:
    VFAT() {}
    ~VFAT(){}

    // Read first word from the block.
    void readFirstWord(uint64_t word)
    {
        m_position = (int)(0x3f & (word >> 56));
        m_crcCheck = 0xff & (word >> 48);
        m_header = 0xff & (word >> 40);
        m_eventCounter = (uint16_t) (0xff & (word >> 32));
        m_bunchCounter = 0xffff & (word >> 16);
        m_msData = 0xffff000000000000 & (word << 48);
    }
    
    // Read second word from the block.
    void readSecondWord(uint64_t word)
    {
        m_msData = m_msData | (0x0000ffffffffffff & word >> 16);
        m_lsData = 0xffff000000000000 & (word << 48);
    }
    
    // Read third word from the block.
    void readThirdWord(uint64_t word)
    {
        m_lsData = m_lsData | (0x0000ffffffffffff & word >> 16);
        m_crc = 0xffff & word;
    }
    
    uint16_t position() { return m_position; }
    uint8_t crcCheck() { return m_crcCheck; }
    uint8_t header() { return m_header; }
    uint8_t eventCounter() { return m_eventCounter; }
    uint16_t bunchCounter() { return m_bunchCounter; }
    uint64_t lsData() { return m_lsData; }
    uint64_t msData() { return m_msData; }
    uint16_t crc() { return m_crc; }

private:
    uint8_t m_position; // An 8bit value indicating the VFAT position on this GEB. 
    uint8_t m_crcCheck;
    uint8_t m_header;
    uint8_t m_eventCounter; // 8 bits.
    uint16_t m_bunchCounter; // 16 bits.
    uint64_t m_lsData; // Channels from 1 to 64.
    uint64_t m_msData; // Channels from 65 to 128.
    uint16_t m_crc; // Check Sum value, 16 bits.
};

#endif

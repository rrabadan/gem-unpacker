#ifndef AMCEVENT_H
#define AMCEVENT_H

#include <cstdio>
#include <vector>

#include "GEMDataEvent.h"
#include "GEB.h"

class AMCEvent: public GEMDataEvent {
public:
    AMCEvent(unsigned int runNumber, unsigned int evtNumber, unsigned int evtSize): GEMDataEvent(runNumber, evtNumber, evtSize) {};
    ~AMCEvent() { m_gebs.clear(); }

    void unpack(uint64_t *words);

    uint8_t slot() { return m_slot; } 
    uint16_t eventCount() { return m_eventCount; }
    uint16_t bunchCount() { return m_bunchCount; }
    uint32_t eventLenHdr() { return m_eventLenHdr;} 

    uint8_t format() { return m_format; }
    uint16_t orbitCount() { return m_orbitCount; }
    uint16_t swSlot() { return m_swSlot; }
    uint16_t swFED() { return m_swFED; }
    
    uint32_t crc() { return m_crc; }
    uint32_t eventLenTrl() { return m_eventLenTrl;} 
    uint16_t eventCountTrl() { return m_eventCountTrl; }

    uint32_t davList () { return m_davList; }
    uint8_t davCount() { return m_davCount; }
    uint8_t gemFormat() { return m_gemFormat; }
    uint8_t vpType() { return m_vpType; }
    uint8_t tts() { return m_tts; }

    uint32_t linkTMO() { return m_linkTMO; }
    uint8_t runType() { return m_runType; }
    uint32_t runParams() { return m_runParams; }
    bool backPressure() { return m_backPressure; }
    bool mmcmLocked() { return m_mmcmLocked; }
    bool daqClockLocked() { return m_daqClockLocked; }
    bool daqReady() { return m_daqReady; }
    bool bc0Locked() { return m_bc0Locked; }
    bool l1aF() { return m_l1aF; }
    bool l1aNF() { return m_l1aNF; }

    void addGEB(GEB geb) { m_gebs.push_back(geb); }
    void clearGEBs() { m_gebs.clear(); }
    const std::vector<GEB> gebs() const { return m_gebs; }

private:
    void setAMCheader1(uint64_t word);
    void setAMCheader2(uint64_t word);
    void setAMCtrailer(uint64_t word);
    void setGEMheader(uint64_t word);
    void setGEMtrailer(uint64_t word);

    uint8_t m_slot;
    uint32_t m_eventCount;
    uint16_t m_bunchCount;
    uint32_t m_eventLenHdr;

    uint8_t m_format;
    uint16_t m_orbitCount;
    uint16_t m_swSlot;
    uint16_t m_swFED;

    uint32_t m_crc;
    uint32_t m_eventCountTrl;
    uint32_t m_eventLenTrl;

    uint32_t m_davList; // Bitmask indicating which inputs/chambers have data.
    uint8_t m_davCount; // Number of chamber blocks.
    uint8_t m_gemFormat;
    uint8_t m_vpType; // VFAT paylod type
    uint8_t m_tts;

    uint32_t m_linkTMO; 
    uint8_t  m_runType;
    uint32_t m_runParams;
    bool m_backPressure;
    bool m_mmcmLocked;
    bool m_daqClockLocked;
    bool m_daqReady;
    bool m_bc0Locked;
    bool m_l1aF;
    bool m_l1aNF;
    
    std::vector<GEB> m_gebs;
};

#endif

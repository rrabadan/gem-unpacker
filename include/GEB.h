#ifndef GEB_H
#define GEB_H

#include <vector>
#include "VFAT.h"

class GEB {
public:
    GEB() {};

    void setChamberHeader(uint64_t word)
    {
        m_calibChan = 0x00ffffff & (word >> 40);
        m_inputId = (int) (0b00011111 & (word >> 35)); // GLIB Input ID.
        m_vfatWdCnt = 0x0fff & (word >> 23); // VFAT word count.
        m_flags = 0b0001111111111111 & (word >> 10); // Thirteen Flags.
        m_evtF = m_flags >> 12;
        m_inF = 0x1 & (m_flags >> 11);
        m_evtSzOFW = 0x1 & (m_flags >> 9);
        m_evtNF = 0x1 & (m_flags >> 8);
        m_inNF = 0x1 & (m_flags >> 7);
        m_evtSzW = 0x1 & (m_flags >> 5);
        m_invalid = 0x1 & (m_flags >> 4);
        m_ooscAvV = 0x1 & (m_flags >> 3);
        m_ooscVvV = 0x1 & (m_flags >> 2);
        m_bxmAvV = 0x1 & (m_flags >> 1);
        m_bxmVvV = 0x1 & m_flags;
    }

    void setChamberTrailer(uint64_t word)
    {
        m_vfatWdCntTr = 0x0fff & (word >> 52); // VFAT word count.
        m_inUfw = 0x01 & (word >> 51); // InFIFO underflow.
        m_zsMask = 0xffffff & (word >> 24);
        m_vfatMask = 0xffffff & word;
    }

    uint8_t calibChan() { return m_calibChan; }
    uint8_t inputId() { return m_inputId; }
    uint16_t vfatWdCnt() { return m_vfatWdCnt; } // Returns VFAT word count (size of VFAT payload)
    uint16_t flags() { return m_flags; } // Returns thirteen flags in GEM Chamber Header

    bool evtF() { return m_evtF; }
    bool inF() { return m_inF; }
    bool evtSzOFW() { return m_evtSzOFW; }
    bool evtNF() { return m_evtNF; }
    bool inNF() { return m_inNF; }
    bool evtSzW() { return m_evtSzW; }
    bool invalid() { return m_invalid; }
    bool ooscAvV() { return m_ooscAvV; }
    bool ooscVvV() { return m_ooscVvV; }
    bool bxmAvV() { return m_bxmAvV; }
    bool bxmVvV() { return m_bxmVvV; }

    uint16_t vfatWdCntTr() { return m_vfatWdCntTr; }  // Returns VFAT word count
    uint8_t inUfw() { return m_inUfw; } // Returns InFIFO underflow flag
    uint32_t zsMask() { return m_zsMask; } // Returns Zero Suppression flags
    uint32_t vfatMask() { return m_vfatMask; } // Returns Zero Suppression flags

    void addVFAT(VFAT v) { m_vfats.push_back(v); }
    
    std::vector<VFAT> vfats() { return m_vfats; }  

private:
    std::vector<VFAT> m_vfats; // Vector of VFAT data.

    uint8_t m_calibChan;
    uint8_t m_inputId; // GLIB input ID (starting at 0)
    uint16_t m_vfatWdCnt;  // Size of VFAT payload in 64 bit words
    uint16_t m_flags; 
    bool m_evtF;
    bool m_inF;
    bool m_evtSzOFW;
    bool m_evtNF;
    bool m_inNF;
    bool m_evtSzW;
    bool m_invalid;
    bool m_ooscAvV;
    bool m_ooscVvV;
    bool m_bxmAvV;
    bool m_bxmVvV;

    uint16_t m_vfatWdCntTr; // Same as in header. This one actually counts the number of valid words 
    uint8_t m_inUfw; // Input status (critical): Input FIFO underflow occured while sending this event
    uint32_t m_zsMask; // Bitmask indicating if certain VFAT blocks have been zero suppressed. 
    uint32_t m_vfatMask; // Bitmask indicating if certain VFAT is enabled
};

#endif

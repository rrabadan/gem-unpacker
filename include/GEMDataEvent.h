#ifndef GEMDATA_INTERFACE
#define GEMDATA_INTERFACE

#include <cstdio>
#include <iostream>

class GEMDataEvent{
public:
    GEMDataEvent(const int runNumber, const int evtNumber, const int evtSize)
        : m_runNumber(runNumber)
        , m_evtNumber(evtNumber)
        , m_evtSize(evtSize)
    {
    }
    int runNumber() { return m_runNumber; }
    int evtNumber() { return m_evtNumber; }
    int evtSize() { return m_evtSize; }

    virtual void unpack(uint64_t *words) = 0;

private:
    const int m_runNumber;
    const int m_evtNumber;
    const int m_evtSize;
};

#endif
